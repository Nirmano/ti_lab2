class Spaceship extends GraphicObject 
{ 
  // vitesse maximum et ralentissement
  float friction = 0.995;
  float topspeed = 6;
  float size = 16;
  float direction = 0;

  Spaceship() 
  {
    location = new PVector(width/2,height/2);
    velocity = new PVector();
    acceleration = new PVector();
  } 

  void update(float deltaTime) 
  { 
    velocity.add(acceleration);
    velocity.mult(friction);
    velocity.limit(topspeed);
    location.add(velocity);
    acceleration.mult(0);
  }

  // Newton's law: F = M * A
  void applyForce(PVector force) 
  {
    PVector f = force.get();
    //f.div(mass); // ignoring mass right now
    acceleration.add(f);
  }
  
  
//******************************
// faire tourner le vaisseau
//******************************
  void turn(float angle) 
  {
    direction += angle;
  }
  
//******************************  
// faire avancer le vaisseau
//******************************
  void thrust() 
  {
    // Offset the angle since we drew the ship vertically
    float angle = direction - PI/2;
    // Polar to cartesian for force vector!
    PVector force = new PVector(cos(angle),sin(angle));
    force.mult(0.1);
    applyForce(force); 
  }

//******************************  
//  téléportation au extrémité
//******************************
  void wrapEdges() 
  {
    float buffer = size*2;
    if (location.x > width + buffer) 
    {
      location.x = - buffer;
    }
    else if (location.x < - buffer)
    {
      location.x = width + buffer;
    }
    
    if (location.y > height + buffer)
    {
      location.y = - buffer;
    }
    else if (location.y < - buffer)
    {
      location.y = height + buffer;
    }
  }
//******************************  
// mort
//******************************
boolean dead(Mover m)
{
  if(PVector.dist(m.location,location) <= m.r + this.size)
  {
    return true;
  }
  else
  {
    return false;
  }
}


  void display() 
  { 

    pushMatrix();
      translate(location.x,location.y+size);
      rotate(direction);
      fill(255,0,0);
      // A triangle
      ellipse(0,4,size*3,size*3);
      fill(0,255,0);
      beginShape();
        vertex(-size,size);
        vertex(0,-size);
        vertex(size,size);
      endShape(CLOSE);
      rectMode(CENTER);
    popMatrix();
  }
}
