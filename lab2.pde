int currentTime;
int previousTime;
int deltaTime;
Spaceship ship;
PFont f ; 
 
ArrayList<Mover> flock;
int flockSize = 50;
int points;
boolean t;
boolean debug = false;

void setup () {
  fullScreen(P2D);
  t = true;
  points = 0;
  ship = new Spaceship();
  currentTime = millis();
  previousTime = millis();
 f = createFont("Arial",36,true);
  flock = new ArrayList<Mover>();
  
  for (int i = 0; i < flockSize; i++) {
    Mover m = new Mover(new PVector(((width/2)-(width/3)),((height/2)-(height/3))), new PVector(random (-2, 2), random(-2, 2)));
    m.fillColor = color(random(255), random(255), random(255));
    flock.add(m);
  }

  flock.get(0).debug = true;
}

void draw () {
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;

  
  update(deltaTime);
    action();
  display();  
}

/***
  The calculations should go here
*/
void update(int delta) {
  
  for (Mover m : flock) {
    m.flock(flock);
    m.update(delta);
  }
}

/***
  The rendering should go here
*/
void display () {
  background(0);
//******************************  
//  affichage ennemie
//******************************
  for (Mover m : flock) 
  {
    m.display();
    if(ship.dead(m))
    {
      t = false;
    }
  }
//******************************  
//  fin de game
//******************************   
  if(!t)
  {
    fill(179,45,0);
    textFont(f,80);
    text("YOU DIED", width/2-150, height/2);
  }
  else if(points > 49)
 {
   fill(179,45,0);
    textFont(f,80);
    text("YOU WIN", width/2-150, height/2);
 }
  else
  {
//******************************  
//  affichage vaisseau
//******************************
    ship.update(deltaTime);
    ship.wrapEdges();
    ship.display();
  }
  fill(0,255,0);
  textFont(f,36);
  text((points+"/50"),50, 50);

 
}

void action()
{
  if (keyPressed) 
  {
    if (key == 'a') 
    {
      //******************************  
      //  tourner a gauche
      //******************************
      ship.turn(-0.03);
    } 
    else if (key == 'd') 
    {
      //******************************  
      //  tourner a droite
      //******************************
      ship.turn(0.03);
    }  
    else if (key == 'w') 
    {
      //******************************  
      //  avancer
      //******************************  
      ship.thrust(); 
    }
    else if (key == 'r') 
    {
      //******************************  
      //  reset
      //******************************        
      setup();
    }
    else if (key == 32) 
    {
      //******************************  
      //  tirer projectille
      //******************************       
      points ++;
    }
  }
}
